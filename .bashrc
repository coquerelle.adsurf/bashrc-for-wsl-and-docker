# Add this must add this at the end of your .bashrc
# replace %username% by your username

# WSL - Docker


if [ -z $(docker-machine.exe ip docker) ]; then
  read -p "need to use docker? " -n 1 -r
  echo " "
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    docker-machine.exe start docker
    export DOCKER_HOST=tcp://$(docker-machine.exe ip docker):2376
    export DOCKER_CERT_PATH=/mnt/c/Users/%username%/.docker/machine/machines/docker
    export DOCKER_MACHINE_NAME=docker
    export DOCKER_TLS_VERIFY=1
    echo "Docker host IP : $(docker-machine.exe ip docker)"
  fi
elif [ -n $(docker-machine.exe ip docker) ]; then
  export DOCKER_HOST=tcp://$(docker-machine.exe ip docker):2376
  export DOCKER_CERT_PATH=/mnt/c/Users/%username%/.docker/machine/machines/docker
  export DOCKER_MACHINE_NAME=docker
  export DOCKER_TLS_VERIFY=1
  echo "Docker host IP : $(docker-machine.exe ip docker)"
fi
