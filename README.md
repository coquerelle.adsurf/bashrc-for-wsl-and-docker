# Bashrc for wsl and docker

After following this tutorial (https://mondedie.fr/d/10494-Faire-fonctionner-docker-sur-Windows-Subsystem-for-Linux), I wondered if I needed docker every time I need WSL ...

To connect the vm docker to our bash shell of WSL Ubuntu (or WSL Ubuntu compatible).
For this you must add this at the end of your .bashrc (instead of its line advised in this tutorial)

Don't forget to close the VM with docker-machine.exe stop docker when you no longer need it.
